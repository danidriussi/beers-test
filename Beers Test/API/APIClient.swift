//
//  APIClient.swift
//  Beers Test
//
//  Created by Daniel Novio on 09/05/19.
//  Copyright © 2019 Daniel Novio. All rights reserved.
//

import Foundation
import Alamofire

public enum APIResult<T: Decodable> {
    case success(T)
    case failure(String)
}

class APIClient {
    
    static let instance = APIClient()
    
    let basePath = "https://api.punkapi.com/v2"
    
    func get<T: Decodable>(from path: String!, result: @escaping (APIResult<[T]>) -> Void) {
        guard let path = path else {
            result(.failure("Informe o path"))
            
            return
        }
        
        guard let url = URL(string: basePath + path) else {
            result(.failure("Problema ao montar URL"))
            
            return
        }
        
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.prettyPrinted, headers: nil).responseObject(response: { (objects: [T]?) in
            guard let objects = objects else {
                result(.failure("Request funcionou, porém não temos resultados =("))
                
                return
            }
            
            result(.success(objects))
            
        }) { (errorMessage: String?) in
            result(.failure(errorMessage ?? "Erro genérico"))
        }
    }
}

extension DataRequest {
    
    func responseObject<T: Decodable>(response: @escaping (_ objects: [T]?) -> Void, error: @escaping (_ errorMessage: String?) -> Void) {
        validate().responseJSON(queue: nil, options: .mutableContainers) { (dataResponse) in
            if dataResponse.result.isFailure {
                error(dataResponse.result.error?.localizedDescription ?? "Erro HTTP genérico")
                
                return
            }
            
            guard let jsonData = dataResponse.data else {
                error("JSON Data não encontrada na resposta")
                
                return
            }
            
            guard let objects = try? [T].from(jsonData) else {
                error("Ocorreu um erro ao desencodar o objeto do tipo \(T.self)" )
                
                return
            }
            
            response(objects)
        }
    }
}
