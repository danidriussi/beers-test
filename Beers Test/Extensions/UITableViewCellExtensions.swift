//
//  UITableViewCellExtensions.swift
//  Beers Test
//
//  Created by Daniel Novio on 09/05/19.
//  Copyright © 2019 Daniel Novio. All rights reserved.
//

import Foundation
import ObjectiveC
import UIKit

private var tableViewCellAssociationKey: UInt8 = 0

extension UITableViewCell {
    
    @objc dynamic var customObject: Any? {
        get{
            return objc_getAssociatedObject(self, &tableViewCellAssociationKey)
        }
        set {
            objc_setAssociatedObject(self, &tableViewCellAssociationKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}
