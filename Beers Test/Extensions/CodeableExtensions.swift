//
//  CodeableExtensions.swift
//  Beer Test
//
//  Created by Daniel Novio on 09/05/19.
//  Copyright © 2019 Daniel Novio. All rights reserved.
//

import Foundation

extension Encodable {
    func json() throws -> Data {
        let jsonEncoder = JSONEncoder()
        return try jsonEncoder.encode(self)
    }
}

extension Decodable {
    static func from(_ json: Data) throws -> Self {
        let jsonDecoder = JSONDecoder()
        return try jsonDecoder.decode(Self.self, from: json)
    }
}
