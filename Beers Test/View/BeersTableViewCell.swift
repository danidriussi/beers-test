//
//  BeersTableViewCell.swift
//  Beers Test
//
//  Created by Daniel Novio on 09/05/19.
//  Copyright © 2019 Daniel Novio. All rights reserved.
//

import UIKit

import Alamofire
import AlamofireImage

class BeersTableViewCell: UITableViewCell {

    static let cellIdentifier = "BeerCell"
    
    @IBOutlet weak private var name: UILabel?
    @IBOutlet weak private var abv: UILabel?
    @IBOutlet weak private var thumbnail: UIImageView?
    
    @IBOutlet weak private var beerDescription: UILabel?
    @IBOutlet weak private var tagline: UILabel?
    @IBOutlet weak private var ibu: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override var customObject: Any? {
        didSet {
            guard let beer = customObject as? Beer else {
                return
            }
            
            if let name = name {
                name.text = beer.name
            }
            
            if let abv = abv {
                abv.text = "Teor Alcoólico: \(beer.abv ?? 0.0)%"
            }
            
            if let thumbnail = thumbnail {
                thumbnail.downloadImage(from: beer.imageUrl)
            }
            
            if let beerDescription = beerDescription {
                beerDescription.text = beer.description
            }
            
            if let tagline = tagline {
                tagline.text = beer.tagline
            }
            
            if let ibu = ibu {
                let intIbu = Int(beer.ibu ?? 0)
                ibu.text = "IBU: \(intIbu)"
            }
        }
    }
}

extension UIImageView {
    func downloadImage(from path: String?) {
        if let imagePath = path {
            if let url = URL(string: imagePath) {
                Alamofire.request(url).responseImage { (imageResponse) in
                    if let image = imageResponse.result.value {
                        self.image = image
                    }
                }
            }
        }
    }
}
