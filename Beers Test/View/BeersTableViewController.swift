//
//  BeersTableViewController.swift
//  Beers Test
//
//  Created by Daniel Novio on 09/05/19.
//  Copyright © 2019 Daniel Novio. All rights reserved.
//

import UIKit

import RxSwift
import RxCocoa

class BeersTableViewController: UITableViewController {
    
    var activityIndicator: UIActivityIndicatorView!
    
    var beerViewModel = BeerViewModel()
    
    var selectedBeer: Beer?
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUIElements()
        
        beerViewModel.fetchData(withPage: nil)
        setupTableBindings()
        setupLoadMoreBindings()
        setupErrorObserver()
    }
    
    //MARK: UI Setups
    
    func setupUIElements() {
        activityIndicator = UIActivityIndicatorView(style: .white)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        activityIndicator.hidesWhenStopped = true
        
        activityIndicator.startAnimating()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator)
    
    }
    
    //MARK: Table bindings and observers
    
    func setupTableBindings() {
        beerViewModel.loading.bind(to: activityIndicator.rx.isAnimating).disposed(by: disposeBag)
        
        beerViewModel.beers.bind(to: tableView.rx.items(cellIdentifier: BeersTableViewCell.cellIdentifier , cellType: BeersTableViewCell.self)) {  (row,beer,cell) in
            
            cell.customObject = beer
            
            }.disposed(by: disposeBag)
        
        tableView.rx.itemSelected.subscribe(onNext: { (indexPath) in
            if let beerCell = self.tableView.cellForRow(at: indexPath) as? BeersTableViewCell {
                
                self.selectedBeer = beerCell.customObject as? Beer
                
                self.performSegue(withIdentifier: "Beer Details Segue", sender: self)
            }
            
        } ).disposed(by: disposeBag)
        
    }
    
    func setupLoadMoreBindings() {
        tableView.rx.willDisplayCell.subscribe(onNext: ({ (cell,indexPath) in
            
            let numberOfRows = self.tableView.numberOfRows(inSection: indexPath.section)-1
            
            if indexPath.row == numberOfRows {
                self.beerViewModel.pageNumber = self.beerViewModel.pageNumber+1
                self.beerViewModel.fetchData(withPage: "\(self.beerViewModel.pageNumber)")
            }
            
        })).disposed(by: disposeBag)
    }
    
    func setupErrorObserver() {
        beerViewModel.error.observeOn(MainScheduler.instance).subscribe(onNext: {(errorMessage) in
            let alert = UIAlertController(title: "Ocorreu um erro!", message: errorMessage, preferredStyle: .alert)
            
            let retryAction = UIAlertAction(title: "Tentar novamente", style: .default, handler: { (action) in
                self.beerViewModel.fetchData(withPage: "\(self.beerViewModel.pageNumber)")
            })
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
            
            alert.addAction(retryAction)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
            
        }).disposed(by: disposeBag)
        
    }
    
    //MARK: TableView Delegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140.0
    }
    
    //MARK: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Beer Details Segue" {
            let beerTC = segue.destination as! BeerDetailsTableViewController
            
            beerTC.beer = selectedBeer
        }
    }
}
