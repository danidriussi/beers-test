//
//  BeerTableViewController.swift
//  Beers Test
//
//  Created by Daniel Novio on 09/05/19.
//  Copyright © 2019 Daniel Novio. All rights reserved.
//

import UIKit

class BeerTableViewController: UITableViewController {

    var beer: Beer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = beer.name
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    
}
