//
//  Beer.swift
//  Beers Test
//
//  Created by Daniel Novio on 09/05/19.
//  Copyright © 2019 Daniel Novio. All rights reserved.
//

import Foundation

struct Beer: Decodable {
    var id: Int?
    var name: String?
    var tagline: String?
    var description: String?
    var imageUrl: String?
    var abv: Double?
    var ibu: Double?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case tagline
        case description
        case imageUrl = "image_url"
        case abv
        case ibu
    }
}

protocol BeerListing {
    
    var endpoint: String { get set }
}
