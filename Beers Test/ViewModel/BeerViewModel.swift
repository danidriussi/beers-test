//
//  BeerViewModel.swift
//  Beers
//
//  Created by Daniel Novio on 08/05/19.
//  Copyright © 2019 Daniel Novio. All rights reserved.
//

import UIKit

import RxSwift
import RxCocoa

class BeerViewModel : BeerListing {
    
    var endpoint: String = "/beers"
    
    var pageNumber: Int = 1
    
    let apiClient = APIClient.instance
    
    var loading: PublishSubject<Bool> = PublishSubject()
    var error: PublishSubject<String> = PublishSubject()
    
    var beerList: [Beer]!
    var beers: PublishSubject<[Beer]> = PublishSubject()
    
    func fetchData(withPage page: String?) {
        loading.onNext(true)
        
        var path = endpoint
        
        if pageNumber > 1, let page = page {
            path+="?page=" + page
        }
        
        apiClient.get(from: path) { (result: APIResult<[Beer]>) in
            self.loading.onNext(false)
            
            switch result {
            case .success(let objects):
                
                if self.beerList == nil {
                    self.beerList = []
                }
                
                self.beerList.append(contentsOf: objects)
                self.beers.onNext(self.beerList)
                
            case .failure(let errorMessage):
                self.error.onNext(errorMessage)
            }
        }
    }
}
