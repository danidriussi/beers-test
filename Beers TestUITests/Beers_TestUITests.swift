//
//  Beers_TestUITests.swift
//  Beers TestUITests
//
//  Created by Daniel Novio on 09/05/19.
//  Copyright © 2019 Daniel Novio. All rights reserved.
//

import XCTest

class Beers_TestUITests: XCTestCase {
    
    let app = XCUIApplication()

    override func setUp() {
        continueAfterFailure = false

        app.launch()
    }

    override func tearDown() {
    }

    func testSimpleNavigation() {
        XCTAssert(app.tables.cells.count > 0)
        
        app.swipeUp()
        app.swipeUp()
        
        let listBeerCell = app.descendants(matching: .table).element.descendants(matching: .cell).matching(identifier: "BeerCell").firstMatch
        
        listBeerCell.tap()
    }
    
    func testBeerListElementsAvailability() {
        XCTAssert(app.tables.cells.count > 0)
        
        let listBeerCell = app.descendants(matching: .table).element.descendants(matching: .cell).matching(identifier: "BeerCell").firstMatch
        
        let labelQuery = listBeerCell.descendants(matching: .staticText)
        
        let nameLabel = labelQuery.matching(identifier: "beerName").firstMatch
        let abvLabel = labelQuery.matching(identifier: "beerAbv").firstMatch
        
        XCTAssert(nameLabel.exists)
        XCTAssert(abvLabel.exists)
    }
    
    func testBeerDetailsElementsAvailability() {
        XCTAssert(app.tables.cells.count > 0)
        
        app.descendants(matching: .table).element.descendants(matching: .cell).matching(identifier: "BeerCell").firstMatch.tap()
        
        let beerDetailsCell = app.descendants(matching: .table).element.descendants(matching: .cell).matching(identifier: "beerDetailsCell").firstMatch
        
        let labelQuery = beerDetailsCell.descendants(matching: .staticText)

        let nameLabel = labelQuery.matching(identifier: "beerDetailsName").firstMatch
        let taglineLabel = labelQuery.matching(identifier: "beerDetailsTagline").firstMatch
        let abvLabel = labelQuery.matching(identifier: "beerDetailsAbv").firstMatch
        let ibuLabel = labelQuery.matching(identifier: "beerDetailsIbu").firstMatch
        
        XCTAssert(nameLabel.exists)
        XCTAssert(taglineLabel.exists)
        XCTAssert(abvLabel.exists)
        XCTAssert(ibuLabel.exists)
        
        //beerDescriptionCell
        let descriptionCellLabelQuery = app.descendants(matching: .table).element.descendants(matching: .cell).matching(identifier: "beerDescriptionCell").firstMatch.descendants(matching: .staticText)
        
        let descriptionLabel = descriptionCellLabelQuery.matching(identifier: "beerDetailsDescription").firstMatch
        
        XCTAssert(descriptionLabel.exists)
    }
}
