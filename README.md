# Beers

Projeto criado para o teste da Pagseguro

### Features

```
Swift, storyboards, programação reativa, MVVM, protocols. 
```

### Pré-requisitos

```
Xcode 10.1, Swift 4
```

## Frameworks usados

* [Alamofire](https://github.com/Alamofire/Alamofire) - Cuidar das requisições da API
* [AlamofireImage](https://github.com/Alamofire/AlamofireImage) - Download de imagens
* [RxSwift 4.5](https://github.com/ReactiveX/RxSwift) - Para programação reativa

### Funcionamento

Explico abaixo algumas classes e componentes importantes do projeto:

##APIClient

Encapsula as requisições HTTP do projeto. O get retorna assincronamente um objeto result (inspirado no Result do Swift 5). O valor associado ao ao result.success é um genérico do tipo Decodable, no result.failure é uma String para usar de mensagem de erro

##BeerViewModel

View model usada para popular a listagem de cervejas, que bate na APIClient para os dados. Foi usado o tipo PublishSubject do RxSwift para fazer a comunicação com a ViewController. A BeerViewModel implementa o protocolo BeerListing, criado na struct Beer com o intuito de forçar seus implementadores a ter um endpoint padrão.  

##BeersTableViewController

View que faz o bind dos dados da ViewModel e mostra a lista de cervejas.

##BeerDetailsTableViewController

View que mostra os detalhes da cerveja selecionada. Já recebe um objeto pronto da tela anterior e não tem outra interação a não ser a visualização. A opção de usar uma TableViewController nessa tela veio por conta da possibilidade de usar o conceito de Self-sizing cells. Dessa forma, todas as descrições e nomes variados das cervejas cabem na tela sem grande complicação.

##BeerTableViewCell

Célula usada nas duas tabelas. Seus outlets são optionals, assim cada tela pode mostrar somente o que for necessário sem crashar o app. Usado o conceito de customObject - onde cada célula pode receber um objeto de qualquer tipo e "se virar" com ele. Para mais informações: UITableViewCellExtensions.swift na pasta Extensions.

### Autor

* **Daniel Novio**

